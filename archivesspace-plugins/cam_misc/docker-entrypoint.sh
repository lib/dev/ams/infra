#!/usr/bin/env bash
set -euo pipefail

# Generate robots.txt for the public interface. The rails initialization copies
# config/robots.txt into the unpacked rails app files to serve it.
if [[ ${ASPACE_RUN_PUBLIC:?} == true && ${ASPACE_PUBLIC_PROXY_URL:-} ]]; then
    robots_txt="\
User-agent: bingbot
Crawl-delay: 10
Disallow: */pdf$

User-agent: SemrushBot
User-agent: SeekportBot
Disallow: /

User-agent: *
Disallow: */pdf$

Sitemap: ${ASPACE_PUBLIC_PROXY_URL:?}/sitemap-index.xml
"
    cat <<<"${robots_txt:?}" > /opt/archivesspace/config/robots.txt
fi
