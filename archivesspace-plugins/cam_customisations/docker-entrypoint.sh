#!/usr/bin/env bash
set -euo pipefail

# TODO: ensure pwd is the plugin dir when these are executed
ASPACE_THEME_COLOUR=${ASPACE_THEME_COLOUR:-'#106470'}
echo ":root {
  --theme: ${ASPACE_THEME_COLOUR:?};
}" > frontend/assets/colour_theme.css
