# PUI settings

AppConfig[:pui_branding_img] = 'assets/images/university-of-cambridge-logo.svg'
AppConfig[:pui_branding_img_alt_text] = 'University of Cambridge'
AppConfig[:cam_brand_title] = aspace_env("LOCALE_BRAND_TITLE", default: nil)
AppConfig[:cam_environment_name] = aspace_env("CAM_ENVIRONMENT_NAME", default: nil)
AppConfig[:cam_theme_name] = aspace_env("CAM_THEME_NAME", default: nil)
AppConfig[:cam_theme_version_id] = ENV.fetch('ARCHIVESSPACE_CONTAINER_BUILD_SHA', '')

AppConfig[:pui_search_results_page_size] = 25

AppConfig[:pui_enable_staff_link] = true # attempt to add a link back to the staff interface
AppConfig[:pui_staff_link_mode] = 'edit'

AppConfig[:pui_hide][:accessions] = true
AppConfig[:pui_hide][:classifications] = true
AppConfig[:pui_hide][:accession_badge] = true
AppConfig[:pui_hide][:classification_badge] = true

AppConfig[:pui_page_actions_cite] = false
AppConfig[:pui_page_actions_bookmark] = true
AppConfig[:pui_page_actions_request] = false
AppConfig[:pui_page_actions_print] = true

AppConfig[:pui_page_custom_actions] = []
AppConfig[:pui_page_custom_actions] << {
  'record_type' => ['resource', 'archival_object'], # the jsonmodel type to show for
  'label' => 'actions.contact', # the I18n path for the action button
  'icon' => 'fa fa-question-circle fa-3x', # the font-awesome icon CSS class
  'url_proc' => proc {|record| 'mailto:'+(('maps@lib.cam.ac.uk' if record.resolved_repository['repo_code'] == 'CUL' && record.identifier.include?('GBR/3296/')) || ('mss@lib.cam.ac.uk' if record.resolved_repository['repo_code'] == 'CUL') || record.repository_information.fetch('email', 'ams@lib.cam.ac.uk'))+'?subject=Enquiry concerning '+record.identifier}
}

AppConfig[:pui_expand_all] = false

# Frontend settings

AppConfig[:locale] = :en
AppConfig[:report_page_layout] = "A4"
AppConfig[:session_expire_after_seconds] = 72000
AppConfig[:default_page_size] = 25

AppConfig[:use_human_readable_URLs] = false
AppConfig[:use_human_readable_urls] = false

AppConfig[:repo_name_in_slugs] = false
AppConfig[:auto_generate_slugs_with_id] = false
AppConfig[:generate_resource_slugs_with_eadid] = false
AppConfig[:generate_archival_object_slugs_with_cuid] = false

AppConfig[:help_enabled] = true
AppConfig[:help_url] = "http://docs.archivesspace.org"
AppConfig[:help_topic_prefix] = "/Default_CSH.htm#"

AppConfig[:feedback_url] = aspace_env('FEEDBACK_URL', default: "mailto:archivesspace@lib.cam.ac.uk")
AppConfig[:pui_feedback_url] = aspace_env('PUI_FEEDBACK_URL', default: "https://www.lib.cam.ac.uk/archivesearch-feedback-form")
AppConfig[:google_analytics_id] = aspace_env('GOOGLE_ANALYTICS_ID', default: "")

AppConfig[:record_inheritance] = {
  :archival_object => {
    :inherited_fields => [
      {
        :property => 'title',
        :inherit_directly => aspace_env('INHERIT_ARCHIVAL_OBJECT_TITLE_FIELD', default: false, tx: method(:parse_bool)),
      },
      {
        :property => 'component_id',
        :inherit_directly => false
      },
      {
        :property => 'linked_agents',
        :inherit_if => proc {|json| json.select {|j| j['role'] == 'creator'} },
        :inherit_directly => false
      },
      {
        :property => 'notes',
        :inherit_if => proc {|json| json.select {|j| j['type'] == 'accessrestrict'} },
        :inherit_directly => aspace_env('INHERIT_ARCHIVAL_OBJECT_NOTES_FIELD', default: false, tx: method(:parse_bool))
      },
      {
        :property => 'notes',
        :inherit_if => proc {|json| json.select {|j| j['type'] == 'scopecontent'} },
        :inherit_directly => false
      }
    ]
  }
}

# Rebecca requested custom report templates be enabled with the 3.4.1 release.
AppConfig[:enable_custom_reports] = aspace_env(
  "ENABLE_CUSTOM_REPORTS", default: true, tx: method(:parse_bool)
)
