if (AppConfig[:run_frontend] or AppConfig[:run_backend]) and (
  aspace_env("ENABLE_CAM_AUTHENTICATION", default: false, tx: method(:parse_bool)))

  # oauth_shared_secret does not need to be set when there's one JVM containing
  # the frontend and backend.
  if not (AppConfig[:run_frontend] and AppConfig[:run_backend])
    setting(:oauth_shared_secret, "OAUTH_SHARED_SECRET")
  end

  entity_id = URI.join(AppConfig[:frontend_proxy_url], '/shibboleth').to_s
  default_idp_cert = \
    "MIICujCCAaICCQDN9BMM2g2oWzANBgkqhkiG9w0BAQUFADAfMR0wGwYDVQQDExRz"\
    "aGliLnJhdmVuLmNhbS5hYy51azAeFw0xNTExMjAxNDUwNTFaFw0yNTExMTcxNDUw"\
    "NTFaMB8xHTAbBgNVBAMTFHNoaWIucmF2ZW4uY2FtLmFjLnVrMIIBIjANBgkqhkiG"\
    "9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxBNt1CZhNwQfCTD7sT0VctwAsdHAqhOmADg+"\
    "Jkpw27QKxVIPlUANAY3e7mbKuWGNYjLv9+KUrkwGhSXnOwUUCC01w+8JpII2j1W6"\
    "8iAvKGszfolVfmfj8vqscQ/UqlSKaGjruWk394v3b7eddYh7HCAOtgOJDIKX9F0e"\
    "bMkIdqQgw2e5uenwt1S9TgwOvYi+IfuZ5yhQv9Yuwo76QS8UkxOyvZdRZl7MIchx"\
    "O0THaTYbrca0GsSc+r9SIb++rM5fQ0yxQzh36PqbGiA1noS/dhkRZb3ywLPNoFzu"\
    "qwWOvcN6ubhO5YOKmTPn1N0uVg94LVMCxMWlO+DjZ8aFmMr96wIDAQABMA0GCSqG"\
    "SIb3DQEBBQUAA4IBAQBimCfClavq2Wk1Zsq9AQ3TWeVFrm1kaCUi4J5j3uWNlMVK"\
    "PsIGE0BHAALMixG+XWt5+QW70CXq6RnHXMS0TLfM5q6K8jIVURK599bTF2/d4fNq"\
    "3QJNaVusuqCqym3Z7rt71QfGtPi0rVKVlQL+lL87a0TDLIyWLsbEe786NpYe0mEe"\
    "BXPQwpPwSaJ1PnPNlsl5i/cUZou5zZQGHtqEY/PR7wAxS/28A6qWLVpMQEUYtb9M"\
    "ZBb6lO15RJ5qwk6paQG87nhMPAFwSbK+OpCkt3hYd7l8LjXNG74eOZdPM5V6DmZz"\
    "nMRF0t4QBDKsuZ64N/+u7R3Nj6uzsQsb7PJXGNTf"

  config = {
    :idp_cert                       => aspace_env("CAM_AUTH_IDP_CERT", default: default_idp_cert),
    :idp_cert_fingerprint           => aspace_env("CAM_AUTH_IDP_CERT_FINGERPRINT", default: "BB:E3:DC:97:68:29:94:04:9D:A3:8B:A0:B8:BF:68:0D:64:76:FE:8A"),
    :idp_sso_target_url             => aspace_env("CAM_AUTH_IDP_SSO_TARGET_URL", default: 'https://shib.raven.cam.ac.uk/idp/profile/SAML2/Redirect/SSO'),
    :path_prefix                    => AppConfig[:frontend_proxy_prefix] + 'auth',
    :assertion_consumer_service_url => URI.join(AppConfig[:frontend_proxy_url], '/auth/saml/callback').to_s,
    :issuer                         => entity_id,
    :sp_entity_id                   => entity_id,
    :certificate                    => aspace_env("CAM_AUTH_SP_CERT"),
    :private_key                    => aspace_env("CAM_AUTH_SP_CERT_PRIVATE_KEY"),
    :name_identifier_format         => "urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress",
    :attribute_statements           => {
      name: ["urn:oid:2.16.840.1.113730.3.1.241"],
      last_name: ["urn:oid:2.5.4.4"],
      email: ["urn:oid:0.9.2342.19200300.100.1.3"],
      description: ["urn:oid:2.5.4.12"],
      phone: ["urn:oid:2.5.4.20"],
    },
  } if AppConfig[:run_frontend]

  # Use the uid field from the omniauth metadata rather than email. Email may be
  # user-specified, whereas uid is based on CRSID (for typical University users)
  AppConfig[:oauth_idtype] = :uid

  AppConfig[:authentication_sources] = [
    {
      model: 'ASOauth',
      provider: 'saml',
      label: 'Raven login',
      slo_link: false,
      config: config,
    },
  ]

  AppConfig[:raven_redirect] = true
else
  # We're not using shib
  AppConfig[:raven_redirect] = false
  # Disable the oauth plugin, as it errors if no :authentication_sources are
  # configured.
  plugin_name = File.basename(File.dirname(__dir__))
  AppConfig[:plugins].delete(plugin_name)
end
