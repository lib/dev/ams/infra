# Cambridge University Libraries — Archive Management System (AMS) Container Images

[Cambridge AMS]: https://archivesearch.lib.cam.ac.uk/
[ArchivesSpace]: https://archivesspace.org/

This repository defines the container images that the [Cambridge AMS] is
deployed with.

The image build is defined in two parts:

1. The generic [ArchivesSpace] container image build in the `main` branch.
   - This is a git submodule in the
     [`./archivesspace-container/`](./archivesspace-container) dir
2. The Cam AMS plugin configuration that we customise stock ArchivesSpace with
   - This is under the [`./archivesspace-plugins/`](./archivesspace-plugins) dir

## Building

The image builds automatically via our GitLab CI. To build manually:

- Allow [direnv](https://direnv.net/) in the repo dir, or alternatively,
  manually export the environment variables that the [.envrc](./.envrc) file
  exports.
- Change directory to `./archivesspace-container`
- The archivesspace and solr images are configured with a docker bake file. To
  build, run `$ docker buildx bake`

## Configuration

Our ams image has configuration options for our plugins in addition to the
[configuration options of the vanilla archivesspace image](./archivesspace-container/archivesspace/README.md).

### SAML / Shibboleth authentication

To enable SAML / Shibboleth authentication for `shib.raven.cam.ac.uk`, there are
two steps:

1. Configure the container
2. Submit the Service Provider XML metadata to `shib.raven.cam.ac.uk` to enable
   the site.

#### SAML / Shibboleth authentication — Part 1: Container configuration

set these envars for the container running the "frontend" service:

- `ASPACE_ENABLE_CAM_AUTHENTICATION=true`
- `ASPACE_CAM_AUTH_SP_CERT_FILE=/run/secrets/saml_sp_cert`
- `ASPACE_CAM_AUTH_SP_CERT_PRIVATE_KEY_FILE=/run/secrets/saml_sp_key`

The `_FILE` envars can point anywhere. They point to the SAML Service Provider
(SP) credentials, which need to be generated. The credentials consist of a
self-signed x509 certificate and a corresponding private key. Generate them
using the `shib-keygen` program (available from the Ubuntu package
[`shibboleth-sp-utils`](https://launchpad.net/ubuntu/focal/+package/shibboleth-sp-utils).

```bash
$ shib-keygen -o . -u "$(id -u)" -g "$(id -g)" \
   -h arcspace-next.lib.cam.ac.uk \
   -e https://arcspace-next.lib.cam.ac.uk/shibboleth

# The credentials are generated in the current dir:
$ ls
sp-cert.pem  sp-key.pem
# You can view the cert like this:
$ openssl x509 -in sp-cert.pem -text
```

The entity ID (`-e`) must use this
`https://<frontend-proxy-hostname>/shibboleth` format, as that's what the
container configuration auto-creates based on the hostname. (It's also the
format used by default by `shib-metagen`, so it seems to be a convention.)

#### SAML / Shibboleth authentication — Part 2: SP metadata

Start the frontend server, then fetch the metadata from `/auth/saml/metadata`,
e.g. `$FRONTEND_PROXY_URL/auth/saml/metadata`.

Create a registration for the deployment at https://metadata.raven.cam.ac.uk/.
It will ask for the metadata.

Once the site is registered, authentication should work without any further
action.

##### Example SAML Metadata

The SAML metadata will be similar to this:

```XML
<?xml version='1.0' encoding='UTF-8'?>
<md:EntityDescriptor ID='_0897bc16-dbf2-4d9a-bc85-43fefbb48a86'
    entityID='http://localhost:8080/shibboleth' xmlns:md='urn:oasis:names:tc:SAML:2.0:metadata'
    xmlns:saml='urn:oasis:names:tc:SAML:2.0:assertion'>
    <md:SPSSODescriptor AuthnRequestsSigned='false' WantAssertionsSigned='false'
        protocolSupportEnumeration='urn:oasis:names:tc:SAML:2.0:protocol'>
        <md:KeyDescriptor use='signing'>
            <ds:KeyInfo xmlns:ds='http://www.w3.org/2000/09/xmldsig#'>
                <ds:X509Data>
                    <ds:X509Certificate>
                        ** Long base64-encoded string... **
                    </ds:X509Certificate>
                </ds:X509Data>
            </ds:KeyInfo>
        </md:KeyDescriptor>
        <md:NameIDFormat>urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress</md:NameIDFormat>
        <md:AssertionConsumerService Binding='urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST'
            Location='http://localhost:8080/auth/saml/callback' index='0' isDefault='true' />
        <md:AttributeConsumingService index='1' isDefault='true'>
            <md:ServiceName xml:lang='en'>Required attributes</md:ServiceName>
            <md:RequestedAttribute FriendlyName='Email address' Name='email'
                NameFormat='urn:oasis:names:tc:SAML:2.0:attrname-format:basic' isRequired='false' />
            <md:RequestedAttribute FriendlyName='Full name' Name='name'
                NameFormat='urn:oasis:names:tc:SAML:2.0:attrname-format:basic' isRequired='false' />
            <md:RequestedAttribute FriendlyName='Given name' Name='first_name'
                NameFormat='urn:oasis:names:tc:SAML:2.0:attrname-format:basic' isRequired='false' />
            <md:RequestedAttribute FriendlyName='Family name' Name='last_name'
                NameFormat='urn:oasis:names:tc:SAML:2.0:attrname-format:basic' isRequired='false' />
        </md:AttributeConsumingService>
    </md:SPSSODescriptor>
</md:EntityDescriptor>
```

### Misc

- `ASPACE_CAM_THEME_NAME` — The colour theme to use. Currently just applies to
  the staff UI, but we should make the public UI use this too. Values are names
  of the [Cam brand colours], e.g. `dark-green`. The options are defined in
  [theme.css].
- `ASPACE_CAM_ENVIRONMENT_NAME` — A short name, like "QA" or "Dev" displayed
  after the site title in the staff UI to indicate which deployment someone is
  looking at. Can be empty/not set, in which case nothing is displayed.

[Cam brand colours]:
  https://www.cam.ac.uk/brand-resources/guidelines/typography-and-colour/colour-palette
[theme.css]:
  https://gitlab.developers.cam.ac.uk/lib/dev/ams/CAM_frontend/-/blob/colour-theme/frontend/assets/theme.css#L4
